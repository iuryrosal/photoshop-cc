# Capítulo 02 - Introduzindo as primeiras ferramentas
O objetivo desse seção é apresentar as primeiras e principais ferramentas do photoshop, no caso, que envolvem movimentação, zoom, desfazer e refazer ações, entre outras. ~~Sem essas ferramentas do que seria o Photoshop?~~
## Aula 01 - Primeiros Movimentos
Nesta aula, falaremos da *Move Tool* que é a ferramenta do Photoshop voltada para movimentação, é com ela que você conseguirá movimentar o conteúdo das camadas, por exemplo. Você pode acessá-la pela *Tool Bar* (Barra de Ferramentas) ou usando a tecla de atalho *V*. 

Tendo a ferramenta em mãos, vamos olhar para o painel de opções dela. A primeira opção é o *Auto Select* (Seleção automática). Estando com ela marcada, o Photoshop irá compreender que quando você clicar em determinado conteúdo do projeto, será automaticamente selecionada a camada em que esse conteúdo se encontra presente. Se essa opção estiver selecionada, o programa sempre deixará marcada a camada que você clicou antes de acessar a ferramenta, mesmo que você clique fora do conteúdo da camada ou em outro conteúdo que pertença  à outra camada.

**Qual a melhor de se usar?**
Na minha opinião, uso normalmente a opção ativada, pois na minha forma de trabalho, com a seleção automática, minha produção é mais rápida e produtiva. Contudo, com algumas pessoas a seleção automática fica desativada. Não existe jeito certo ou errado, basta escolher o que você melhor trabalha.

Continuando no menu de opções, existe uma caixa de seleção: *Layer* (Camada) e *Group* (Grupo), aqui indica o que deseja movimentar com a move tool, uma camada em específica ou um conjunto inteiro de camadas (nesse caso, um grupo). Aí depende do que se deseja movimentar, as vezes em projetos tendo um bloco já montado (tipo uma caixa de diálogo, com informações de texto e com algum símbolo), caso deseje movimentá-lo, para não movimentar camada por camada, tendo que alinhar novamente depois, basta reunir tudo num agrupamento e usar a Move Tool com a opção de seleção Group.

Em seguida, existe a opção *Show Transform Tools*, que se ativada, irá exibir aquela "caixa" com a possibilidade de dimensionar o conteúdo (a mesma quando você utiliza o atalho CTRL + T).

Por fim, vêm várias opções de alinhamento, semelhante aquelas encontradas em outros softwares, como o Word. No caso, alinhamento à direita, alinhamento à esquerda, centralizado, entre outras. Todavida, para essas opções de alinhamento estarem ativadas é necessário estar com, pelo menos, duas camadas selecionadas. Selecionando 3 camadas, simultâneamente, é possível usar outras opções de alinhamento.

No vídeo referente a esta aula, são mostradas, na prática, o funcionamento destas opções, não deixe de assistir!!

## Aula 02 - Desfazendo e Refazendo Ações
É muito comum errarmos, afinal ~~Errar é humano!~~, mas permanecer no erro... já complica. Daí entramos na importância do conhecimento desse procedimento. Devido à última atualização do Photoshop CC 2019, a forma de trabalhar com essas ferramentas mudou, por isso iremos diferenciar entre as versões anteriores e a atual.

**Versões Antigas**
Para desfazer uma ação, utilize o atalho CTRL + Z, mas lembrando que esse atalho só desfaz uma única vez, se aplicar novamente CTRL + Z, o programa irá refazer a ação anteriormente desfeita.

*Iury, mas se eu quiser desfazer ou refazer mais de uma ação?*
Assim, será usada uma outra tecla de atalho CTRL + ALT + Z. Para refazer mais de uma ação, o atalho é CTRL + SHIFT + Z. 

**Versão Nova**
Na versão CC 2019, agora o CTRL + Z apenas desfaz ações e o CTRL + SHIFT + Z refaz ações. O atalho CTRL + ALT + Z não existe mais nessa nova versão.

**Vale para as duas versões**
Essas opções podem ser acessadas manualmente pelo menu superior, na aba EDIT. ~~Mas acredite, os atalhos sempre são as melhores opções kk~~

Mas lembre-se que existe um limite para desfazer e, consequentemente, refazer ações, é possível alterar esse limite mexendo nas preferências do programa. Não exagere, pois dependendo do seu computador, ele só consiga suporta uma determinada quantidade, o recomendado é manter entre 10 à 50 ações salvas (depende da sua máquina). Assim, você consegue desfazer e refazer, no mínimo, 10 ações realizadas. Para modificar essa quantidade, acesse no menu superior ***Edit***(Editar) > ***Preferences*** (Preferências) > ***Peformace*** (Performance). Ao ir nessa opção, você terá um menu e uma opção denominada ***History States***, que em uma tradução seria "Estados da História", o que indica a quantidade de processos que serão armazenados na memória, possibilitando desfazê-los e refazê-los.
 
 Existe uma ferramenta também (~~bem escondida por sinal~~) que trabalha exclusivamente com a volta e  ida de ações. É a ferramenta de ***History*** (Histórico). Você pode acessá-la pelo menu superior (Window) e localizar essa ferramenta. Ao clicar, abre-se uma aba que possui o registro de todas as ações feitas (dentro do limite estipulado). É possível selecionar a ação que deseja voltar, assim você consegue desfazer muitas ações (ou refazer) de uma vez, sem ter que ficar usando o atalho. Se torna mais prático. Uma coisa interessante também é que a ferramente possibilita abrir um segundo projeto repetindo até determinada ação que você realizou no primeiro projeto (no vídeo, isso é mostrado de forma mais clara). As ferramentas de desfazer e refazer ações (os atalhos já apresentados) também podem ser acessados de maneira manual no History. 
  
## Aula 03 - Ferramenta de Zoom
Essa ferramenta será muito útil ao longo do desenvolvimento. Afinal, sempre precisamos de maior precisão (tendo que ver os detalhes de mais de perto) e, também, de uma visualização completa para vermos como nosso projeto está ficando. A ferramenta ***Zoom*** pode ser localizada na barra de ferramentas ou uzando o atalho ***Z***. Com o Z ativado, o programa dar um ZOOM+ no projeto, aproximando mais determinadas regiões. Por meio do menu de opções ou do atalho ***ALT + Z***, você pode retirar o zoom e conseguir ter uma visão mais ampliada do projeto. 

No menu de opções, existem opções ***100%*** (para o zoom ficar 100% do projeto), ***Fill Screen*** (Preencher a Tela), em que o programa dar um zoom de modo que toda a tela da área de trabalho seja preenchida pelo projeto e o ***Fit Screen*** (Reduzir a Tela), o programa irá deixar todo o projeto visível na tela da área de trabalho, mas evitando bordas externas.

Na aba do projeto, onde aparece o nome do projeto, por exemplo, é indicado a % do zoom em que o projeto se encontra (isso é apresentado no vídeo). 

No menu superior em ***View*** (Visualizar), você pode mexer em outras opções de Zoom, com o ***Zoom In*** (Atalho CTRL + +) e ***Zoom Out*** (Atalho CTRL + -). Essas duas opções funcionam de forma semelhante ao Zoom. Mas por que a existência delas? Basicamente, em alguns casos, usando algumas outras ferramentas, não é possível utilizar a ferramenta de Zoom simultaneamente, tendo que utilizar o Zoom In e Zoom Out. Além delas, você encontra na aba View as opções de deixar a visualização em 100%, 200%, Fit Screen, entre outras. (Elas são apresentadas no vídeo em questão).

## Aula 04 - Tamanho da Imagem
Aqui vamos falar de um assunto que é um pouco "avançado" para alguns, ~~ou nem tanto~~. Uma certeza é que muitas pessoas, ao estudarem Photoshop sem seguir um curso ou livro respectivo, dificilmente sabem da existência dessa ferramenta. *O que ela trabalha afinal?* Simplesmente, o tamanho da imagem de forma a preservar a resolução dela. *Resolução?* Sim, resolução. Toda imagem possui a resolução pixels/polegada (lembra dessa opção que usamos na criação de arquivos?). Quanto maior a quantidade de pixels por polegada na imagem, maior é a resolução dela.

Uma imagem é composta por pixels, que são aqueles pequenos quadradinhos que você vê quando amplia uma imagem. Agora, a medida que ampliamos essa imagem, os pixels aumentam, mas aumenta também a distância entre eles, e o que o computador faz é preencher esses espaços criando pixels novos a partir dos “pixels vizinhos”, contudo se isso for feito de forma errada ou brusca o que acontece é que há distorção na imagem. Da mesma forma, se você diminui demais uma imagem a distância dos pixels diminui, o que faz com que os pixels se sobreponham causando um borrão na imagem. Esse ato de trabalhar com os pixels (criando ou eliminando novos), se baseando nos já existentes, durante o processo de dimensionamento é chamado de **Interpolação de Imagem**. A ferramenta de Tamanho de Imagem possibilita trabalharmos melhor com esse processo durante o dimensionamento de uma imagem, evitando perdas na qualidade visual do conteúdo.

Para acessá-la, basta ir na opção, no menu superior, ***Image*** (Imagem) > ***Image Size***(Tamanho de imagem) ou usar o atalho CTRL + ALT + I. Ao clicar na opção, será aberta uma janela.

Nesta janela, existe uma pré-visualização da imagem, informações do seu tamanho e da sua dimensão. Existem presets para aplicações WEB e Impressão. Você pode escolher a unidade para ser trabalhada e a partir do tamanho colocado, a imagem é ampliada ou reduzida. (No vídeo, isso é apresentado). 

Um diferencial dessa janela é a opção ***Resample*** (Redefinir). Estando desativada, ao dimensionar a imagem, o programa irá adaptar a resolução (distribuição de pixels por polegadas), assim a quantidade de dados da imagem permanecerá constante. 

Para casos mais específicos e customizáveis (em que se deseja alteração da quantidade de dados, adicionando ou excluindo pixels), você pode ativar essa opção, podendo definir uma resolução e tamanho desejada, sendo que em seguida irá selecionar como o programa irá trabalhar com a adição ou remoção de pixels (nova quantidade de dados), evitando percas indesejáveis.

Quando você _diminui a resolução_ (reduz o número de pixels), algumas informações são excluídas da imagem. Ao _redefinir uma resolução mais alta_ (aumentar o número de pixels ou _aumentar a resolução_), são adicionados novos pixels. Especifique um método de _interpolação_ para determinar como os pixels serão adicionados ou excluídos.

Lembre-se de que a redefinição da resolução pode gerar uma imagem de qualidade inferior. Por exemplo, quando você redefine a resolução da imagem com dimensões em pixels maiores, a imagem perde alguns detalhes e a nitidez.

O Photoshop redefine a resolução das imagens usando um _método de interpolação_ para atribuir valores de cores a novos pixels com base nos valores de cores dos pixels existentes. A seguir veja as opções disponíveis:

**Pelo mais Próximo**

Um método rápido e menos preciso de reproduzir pixels de uma imagem. Esse método deve ser usado com ilustrações que contenham arestas sem suavização de serrilhado, para preservar as arestas sólidas e produzir um arquivo menor. Entretanto, esse método pode produzir efeitos irregulares, que se tornam visíveis ao distorcer ou redimensionar uma imagem ou ao executar várias manipulações em uma seleção.

**Bilinear**

Um método que adiciona pixels, calculando a média dos valores de cor dos pixels adjacentes. Produz resultados de qualidade média.

**Bicúbico**

Um método mais lento, porém, mais preciso, baseado no exame dos valores dos pixels adjacentes. Por usar cálculos mais complexos, Bicúbico produz gradações tonais mais suaves do que Pelo mais Próximo ou Bilinear.

**Bicúbico mais Suave**

Um bom método para aumentar imagens com base na interpolação Bicúbica, mas desenvolvido para produzir resultados mais suaves.

**Bicúbico mais Nítido**

Um bom método para reduzir o tamanho da imagem com base na interpolação Bicúbica, com nitidez aprimorada. Esse método mantém os detalhes em uma imagem com resolução redefinida. Se Bicúbico mais Nítido tornar nítido demais algumas áreas da imagem, tente usar o Bicúbico.
 
## Aula 05 - Alterando o Tamanho do Projeto
Esta ferramenta é poderosa para alterar o tamanho do projeto depois de criado. Na fase de aplicações, você verá a utilidade desta ferramenta para gerar as famosas margens de segurança, muito comum em um trabalho que envolve impressão. 

Para acessá-la vá em **Image > Canvas Size (ALT + CTRL + C)**. Após isso, se abrirá o painel da ferramenta, você pode escolher a unidade em que será feita a expansão e um espaço para digitar as novas dimensões. Essa expansão pode ocorrer de forma absoluta (para valores inteiros) ou relativa (para valores "quebrados"). Logo abaixo a opção para indicar o sentido dessa expansão: para todos os lados, apenas para a direita ou apenas para a esquerda, entre outras. Além disso, se habilitado (depende de como está as camadas do seu projeto) é possível selecionar como será preenchido esse novo "espaço" que será gerado devido a expansão. Aqui falamos de expansão, expansão, mas lembre que caso você digite uma dimensão menor que a já definida no arquivo, ocorrerá a compressão. Isso pode fazer com que algum conteúdo seja "cortado" do projeto. Portanto, muita cautela no uso desta ferramenta. 
> Written with [StackEdit](https://stackedit.io/).
